const btn = document.querySelector("#search-btn");
btn.addEventListener("click", getInfo);

class UserLocation {
  constructor() {
    this.ip = "";
    this.continent = "";
    this.country = "";
    this.region = "";
    this.city = "";
    this.district = "";
  }

  async getIP() {
    try {
      const response = await fetch("https://api.ipify.org/?format=json");
      const reply = await response.json();
      console.log(reply);
      this.ip = reply.ip;
    } catch (error) {
      console.error(`Error fetching IP address: ${error}`);
    }
  }

  async getLocation() {
    try {
      const requestIP = `http://ip-api.com/json/${this.ip}?fields=continent,country,regionName,city,district`;
      const response = await fetch(requestIP);
      const location = await response.json();
      console.log(location);
      Object.assign(this, location);
    } catch (error) {
      console.error(`Error fetching location data: ${error}`);
    }
  }

  showInfo() {
    try {
      const infoElement = document.querySelector("#info");

      infoElement.insertAdjacentHTML(
        "afterbegin",
        `<ul>
          <li>Континент: ${this.continent}</li>
          <li>Країна: ${this.country}</li>
          <li>Регіон: ${this.region}</li>
          <li>Місто: ${this.city}</li>
          <li>Район: ${this.district}</li>
        </ul>`
      );
    } catch (error) {
      console.error(`Error inserting page info: ${error}`);
    }
  }
}

async function getInfo() {
  const user = new UserLocation();
  await user.getIP();
  await user.getLocation();
  user.showInfo();
}
