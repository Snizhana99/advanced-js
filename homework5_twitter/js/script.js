// class Request {
// 	post(url, formObj) {
// 	  return fetch(url, {
// 		 method: "POST",
// 		 headers: {
// 			"Content-Type": "application/json",
// 		 },
// 		 body: JSON.stringify(formObj),
// 	  }).then((responce) => responce.json());
// 	}
// 	put(url, formObj) {
// 	  return fetch(url, {
// 		 method: "PUT",
// 		 headers: {
// 			"Content-Type": "application/json",
// 		 },
// 		 body: JSON.stringify(formObj),
// 	  }).then((responce) => responce.json());
// 	}
//  }

// class Request {
//   constructor(baseUrl) {
//     this.baseUrl = baseUrl;
//   }

//   async get(url) {
//     const response = await fetch(this.baseUrl + url);
//     return response.json();
//   }

//   async post(url, data) {
//     const response = await fetch(this.baseUrl + url, {
//       method: "POST",
//       body: JSON.stringify(data),
//       headers: {
//         "Content-Type": "application/json",
//       },
//     });
//     return response.json();
//   }

//   async put(url, data) {
//     const response = await fetch(this.baseUrl + url, {
//       method: "PUT",
//       body: JSON.stringify(data),
//       headers: {
//         "Content-Type": "application/json",
//       },
//     });
//     return response.json();
//   }

//   async delete(url) {
//     const response = await fetch(this.baseUrl + url, {
//       method: "DELETE",
//     });
//     return response.json();
//   }

//   async getAll() {
//     const users = this.get("https://ajax.test-danit.com/api/json/users");
//     const posts = this.get("https://ajax.test-danit.com/api/json/posts");
//     const [usersData, postsData] = await Promise.all([users, posts]);
//     return { users: usersData, posts: postsData };
//   }
// }
// ----------------------------------------------
class Request {
  constructor(baseUrl) {
    this.baseUrl = baseUrl;
  }

  get(url) {
    return fetch(this.baseUrl + url)
      .then((responce) => responce.json())
      .catch((error) => {
        console.error(`Error: ${error.message}`);
      });
  }

  post(url, data) {
    return fetch(this.baseUrl + url, {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((responce) => responce.json())
      .catch((error) => {
        console.error(`Error: ${error.message}`);
      });
  }

  put(url, data) {
    return fetch(this.baseUrl + url, {
      method: "PUT",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((responce) => responce.json())
      .catch((error) => {
        console.error(`Error: ${error.message}`);
      });
  }

  //   delete(url) {
  //     return fetch(this.baseUrl + url, {
  //       method: "DELETE",
  //     })
  //       .then((responce) => responce.json())
  //       .catch((error) => {
  //         console.error(`Error: ${error.message}`);
  //       });
  //   }

  delete() {
    fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
      method: "DELETE",
    })
      .then((response) => {
        //   if (response.ok) {
        //     const cardElement = document.querySelector(
        //       `[data-post-id="${this.postId}"]`
        //     );
        //     cardElement.remove();
        //   } else {
        //     throw new Error("Network response was not ok");
        //   }
        const cardElement = document.querySelector(
          `[data-post-id="${this.postId}"]`
        );
        cardElement.remove();
      })
      .catch((error) => console.error("Error:", error));
  }

  getAll() {
    const users = this.get("/users");
    const products = this.get("/posts");
    return Promise.all([users, products])
      .then(([usersData, productsData]) => {
        return { users: usersData, products: productsData };
      })
      .catch((error) => {
        console.error(`Error: ${error.message}`);
      });
  }
}

// ------------------------------------------------------------
class Card {
  constructor(postId, userName, userEmail, title, body) {
    this.postId = postId;
    this.userName = userName;
    this.userEmail = userEmail;
    this.title = title;
    this.body = body;
  }

  render() {
    const card = document.createElement("li");
    card.classList.add("card_user");
    card.innerHTML = `
		<div class="user_title">
			<img src="./img/twitter.png" alt="twitter icon" width="50px" height="50px">
			<h2 class="user_name" >${this.userName}</h2>
			<a class="user_email" href="email:${this.userEmail}">${this.userEmail}</a>
		</div>
		<h3 class="user_post-title">${this.title}</h3>
		<p class="user_post-text">${this.body}</p>
		<button class="edit-post-btn">Редагувати</button>
	 `;
    const deleteButton = document.createElement("a");
    deleteButton.classList.add("card_delete-button");
    deleteButton.innerHTML = `<img src="./img/iconfinder-trash-4341321_120557.png" alt="">`;

    deleteButton.addEventListener("click", () => {
      // this.delete();
      // const requestDel = new Request();
      // requestDel.delete().then((data))
    });
    card.appendChild(deleteButton);

    return card;
  }

  //   delete() {
  //     fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
  //       method: "DELETE",
  //     })
  //       .then((response) => {
  //         if (response.ok) {
  //           const cardElement = document.querySelector(
  //             `[data-post-id="${this.postId}"]`
  //           );
  //           cardElement.remove();
  //         } else {
  //           throw new Error("Network response was not ok");
  //         }
  //       })
  //       .catch((error) => console.error("Error:", error));
  //   }
}

const mainContainer = document.querySelector("#posts");
const cardsWraper = document.createElement("ul");
cardsWraper.classList.add("card_wrap");
mainContainer.appendChild(cardsWraper);

document.body.classList.add("loading");

Promise.all([
  fetch("https://ajax.test-danit.com/api/json/users"),
  fetch("https://ajax.test-danit.com/api/json/posts"),
])
  .then((responses) =>
    Promise.all(responses.map((response) => response.json()))
  )
  .then((data) => {
    document.body.classList.remove("loading");
    const [users, posts] = data;
    console.log(data);
    const cards = posts.map((post) => {
      const user = users.find((user) => user.id === post.userId);
      return new Card(post.id, user.name, user.email, post.title, post.body);
    });

    cards.forEach((card) => {
      const cardElement = card.render();
      cardElement.dataset.postId = card.postId;
      cardsWraper.appendChild(cardElement);
    });
  })
  .catch((error) => {
    console.error(error);
    document.body.classList.remove("loading");
  });
// -------------modal---------------------------
class Modal {
  render() {
    const postModal = document.getElementById("add-post-modal");
    postModal.innerHTML = `<div class="modal-content">
	 <span class="close">&times;</span>
	 <h2>Додати публікацію</h2>
	 <form id="add-post-form">
		 <label for="post-title">Заголовок:</label>
		 <input type="text" id="post-title" name="post-title"><br><br>
		 <label for="post-text">Текст публікації:</label>
		 <textarea id="post-text" name="post-text"></textarea><br><br>
		 <button type="submit">Додати</button>
	 </form>
 </div>`;
    const closeBtn = document.querySelector(".close");
    closeBtn.addEventListener("click", () => {
      addPostModal.style.display = "none";
    });
    const addPostForm = document.getElementById("add-post-form");
    addPostForm.addEventListener("submit", (event) => {
      event.preventDefault();

      const title = document.getElementById("post-title").value;
      const content = document.getElementById("post-text").value;
      // const authorId = 1;

      const postData = {
        title,
        content,
        //   authorId,
      };

      fetch("https://ajax.test-danit.com/api/json/posts", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(postData),
      })
        .then((response) => response.json())
        .then((data) => {
          const newPost = data;
          const postsList = document.querySelector(".card_wrap");
          const card = document.createElement("li");
          card.classList.add("card_user");
          card.innerHTML = `
		  <div class="user_title">
			  <img src="./img/twitter.png" alt="twitter icon" width="50px" height="50px">
			  <h2 class="user_name" >Leanne Graham</h2>
			  <a class="user_email" href="email:Sincere@april.biz">Sincere@april.biz</a>
		  </div>
		  <h3 class="user_post-title">${newPost.title}</h3>
		  <p class="user_post-text">${newPost.content}</p>
		  <button class="edit-post-btn">Редагувати</button>
		`;
          const deleteButton = document.createElement("a");
          deleteButton.classList.add("card_delete-button");
          deleteButton.innerHTML = `<img src="./img/iconfinder-trash-4341321_120557.png" alt="">`;

          //  deleteButton.addEventListener("click", () => {
          //    this.delete();
          //  });
          card.appendChild(deleteButton);
          postsList.insertBefore(card, postsList.firstChild);
          return card;
        })
        .catch((error) => {
          console.error("Помилка при додаванні публікації:", error);
        });
    });
    return postModal;
  }
}

const modalWindow = new Modal();

const addPostBtn = document.getElementById("add-post-btn");
const addPostModal = document.querySelector(".modal");

addPostBtn.addEventListener("click", () => {
  document.body.append(modalWindow.render());
  addPostModal.style.display = "block";
});
